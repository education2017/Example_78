package hr.ferit.brunozoric.example_78;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DisplayStatusActivity extends Activity {
	Button bPostStatus;
	EditText etNewStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_display_status);
		this.setUpUI();
	}

	private void setUpUI() {
		this.bPostStatus = (Button) findViewById(R.id.bPostStatus);
		this.etNewStatus = (EditText) findViewById(R.id.etNewStatus);
		this.bPostStatus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String status = etNewStatus.getText().toString();
				Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
			}
		});

		Intent startingIntent = this.getIntent();
		String action = startingIntent.getAction();
		String type = startingIntent.getType();
		if(action.equals(Intent.ACTION_SEND)){
			if(type.equals("text/plain")){
				if(startingIntent.hasExtra(Intent.EXTRA_TEXT)){
					String status = startingIntent.getStringExtra(Intent.EXTRA_TEXT);
					this.etNewStatus.setText(status);
				}
			}
		}
	}
}
