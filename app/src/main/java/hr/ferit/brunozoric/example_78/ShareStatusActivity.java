package hr.ferit.brunozoric.example_78;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class ShareStatusActivity extends Activity implements View.OnClickListener {

	Button bShareStatus;
	EditText etStatusInput;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share_status);
		this.setupUI();
	}

	private void setupUI() {
		this.bShareStatus = (Button) findViewById(R.id.bShareStatus);
		this.etStatusInput = (EditText) findViewById(R.id.etStatusInput);
		this.bShareStatus.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		String status = this.etStatusInput.getText().toString();
		Intent explicitIntent = new Intent();
		explicitIntent.setAction(Intent.ACTION_SEND);
		explicitIntent.setType("text/plain");
		explicitIntent.putExtra(Intent.EXTRA_TEXT, status);
		if(explicitIntent.resolveActivity(getPackageManager()) != null){
			startActivity(explicitIntent);
		}
	}
}

